//#---#---#---#---#---#---#---#---#---#---#---#---#
//
// BukkitClicker by artur9010
// game.js
//
// --> Credits (Github usernames)
// @artur9010 - for all
// @NorthPL - save mechanism
//
// --> You can copy pieces of code ( or the whole ), but information about this place that I am the author of this piece of code .
//
// Copyright 2015 artur9010 (artur9010.github.io)
// 
//#---#---#---#---#---#---#---#---#---#---#---#---#

var bukkits;
var bukkitsAllTime;
var level;
var exp;

//
// buildings enum
// No desc
//
var buildings = {
	VANILLA: {id: 1, cost: 10, production: 0.9, amount: 0, displayName: "Vanilla"},
	BUKKIT: {id: 2, cost: 100, production: 2.7, amount: 0, displayName: "Bukkit"},
	SPIGOT: {id: 3, cost: 500, production: 5.7, amount: 0, displayName: "Spigot"},
	GLOWSTONE: {id: 4, cost: 2500, production: 38.2, amount: 0, displayName: "Glowstone"},
	SPONGE: {id: 5, cost: 7500, production: 119.4, amount: 0, displayName: "Sponge"},
};

//
// initGame()
// Game initial code.
//
function initGame(){	
	//Define varibbles
    bukkits = 0;
	bukkitsAllTime = 0;
    level = 1;
    exp = 0;
	
	//Load saved game (or not)
    loadGame();
	
	//Create UI
	createUI();
	createButtons();
	
	//Set intervals
    setInterval(function () {update()}, 15);
    setInterval(function () {silnikiTimer()}, 100);
    setInterval(function () {saveGame()}, 30000);
	setInterval(function () {updateProgressBar()}, 750);
	
	//Remove no javascript support message.
	$("#noSupport").html('');
}

//
// saveGame()
// Save the game
//
function saveGame(){
	document.cookie="bukkits="+bukkits;
	document.cookie="bukkitsAllTime="+bukkitsAllTime;
	document.cookie="level="+level;
	document.cookie="exp="+exp;
	
	for(var silnik in buildings){
		if(buildings[silnik]['amount'] != 0){
			document.cookie=buildings[silnik]['displayName'].toLowerCase()+"="+buildings[silnik]['amount'];
			console.log(buildings[silnik]['displayName'].toLowerCase()+"="+buildings[silnik]['amount']);
		}
	}
}


//
// loadGame()
// Load saved game
//
function loadGame(){
    var tempCookies = document.cookie;
    var splittedCookies = tempCookies.split(";");
    splittedCookies.forEach(function(e)
    {
	    e = e.replace(/\s+/g, '');
		var l = e.split("=");
		
	    if(l[0].toLowerCase() == "bukkits")
	    {
	  	    bukkits = parseInt(l[1]);
	    }
		
		if(l[0].toLowerCase() == "bukkitsAllTime")
	    {
	  	    bukkitsAllTime = parseInt(l[1]);
	    }
	  
	    if(l[0].toLowerCase() == "level")
	    {
	  	    level = parseInt(l[1]);
	    }
	  
	    if(l[0].toLowerCase() == "exp")
	    {
	  	    exp = parseInt(l[1]);
	    }
	  
		for(var silnik in buildings){
			if(l[0].toLowerCase() == buildings[silnik]['displayName'].toLowerCase()){
				if(parseInt(l[1]) != 0){
					buildings[silnik]['amount'] = parseInt(l[1]);
					console.log(buildings[silnik]['displayName'] + " - " + parseInt(l[1]));
				}
			}
		}
  });
}

//
// resetGame()
// RESET
//
function resetGame(){
	bukkits = 0;
	bukkitsAllTime = 0;
    level = 1;
    exp = 0;
	for(var silnik in buildings){
		buildings[silnik]['amount'] = 0;	
	}
	saveGame();
}

//
// getBukkits()
// Return bukkits
//
function getBukkits(){
    return bukkits;
}

//
// getLevel()
// Return level
//
function getLevel(){
    return level;
}

//
// getExp()
// Return exp
//
function getExp(){
    return exp;
}

//
// getExpToNextLevel()
// No desc
//
function getExpToNextLevel(){
    return (level * 57.357654364 + 30.127275678589348675).toFixed(2);
}

//
// addBukkits(toAdd)
// No desc
//
function addBukkits( toAdd ){
    bukkits += toAdd;
    exp += toAdd;
	bukkitsAllTime += toAdd;
}

//
// addBukkitsWithoutExp(toAdd)
// No desc
//
function addBukkitsWithoutExp( toAdd ){
    bukkits += toAdd;
	bukkitsAllTime += toAdd;
}

//
// removeBukkits(toRemove)
// No desc
//
function removeBukkits( toRemove ){
    bukkits = bukkits - toRemove;
}

//
// hasBukkits(how)
// No desc
//
function hasBukkits( how ){
    if(bukkits < how || bukkits == how){
        return true;
    }
    return false;
}

//
// canLevelUp()
// No desc
//
function canLevelUp(){
    if(exp == getExpToNextLevel() || exp > getExpToNextLevel()){
        exp = exp - getExpToNextLevel();
        addBukkitsWithoutExp( level*33.68 );
        level++;
    }
}

//
// update()
// Update HTML code
//
function update() {
	canLevelUp()
	$("#bukkits").html("<h2>" + getBukkits().toFixed(1) + " Bukkits</h2>");
	$("#total").html("<b>Collected throughout the game:</b> " + bukkitsAllTime.toFixed(1));
	$("#level").html("<h3>Level:" + getLevel() + "</h3>");
	$("#progress").attr("data-perc", getExpToNextLevelPercent());
	for(var silnik in buildings){
		document.getElementById("buy" + buildings[silnik]['id']).innerHTML = "Buy " + buildings[silnik]['displayName'] + " ! [ <b>" + buildings[silnik]['amount'] + "</b> ] ( " + getCost(buildings[silnik]) + " )";
		//$("#buy" + buildings[silnik]['id']).html("Buy " + buildings[silnik]['displayName'] + " ! [ <b>" + buildings[silnik]['amount'] + "</b> ] ( " + getCost(buildings[silnik]) + " )");
	}
	$(document).prop('title', getBukkits().toFixed(1) + ' Bukkits');
}
//
// silnikiTimer()
// No desc
//
function silnikiTimer(){
	var toAdd = 0;
	
	for(var silnik in buildings){
		toAdd += buildings[silnik]['amount']*buildings[silnik]['production'];
	}
	toAdd = toAdd/10;
	addBukkits(toAdd);
}

//
// getCost(silnik)
// No desc
//
function getCost(silnik){
	return (silnik.cost + (silnik.amount * 1.453 * silnik.cost)).toFixed(2);
}

//
// buy(silnik)
// No desc
//
function buy(silnik){
	if(getCost(silnik) < getBukkits() || getCost(silnik) == getBukkits()){
		removeBukkits(getCost(silnik));
		silnik.amount++;
	}
}
//
// createUI()
// Creates user interface
//
function createUI(){
	// Bukkits info
	$('<div/>', {
		"class": 'whiteText',
		"id": 'bukkits'
	}).appendTo('#centre');
	
	// Total info
	$('<div/>', {
		"class": 'whiteText',
		"id": 'total'
	}).appendTo('#centre');
	
	// Bucket
	$('<img/>', {
		"class": 'bucket',
		"alt": 'Click me :D',
		"src": 'media/bucket.png',
		click: function(){ addBukkits(1); }
	}).appendTo('#centre');
	
	// Level info
	$('<div/>', {
		"id": 'level'
	}).appendTo('#centre');
	
	// Progressbar
	$('<div/>', {
		"class": 'progressbar',
		"id": 'progress',
		"data-perc": '0'
	}).appendTo('#centre');
	
	$("#progress").append("<div class=\"bar color2\"><span></span></div>");
}

//
// createButtons()
// No desc
//
function createButtons(){
	var centre = document.getElementById( "centre" );
	for(var silnik in buildings){
		button = document.createElement( "button" );
		button.id = "buy" + buildings[silnik]['id'];
		button.silnik = buildings[silnik];
		button.onclick = function(){
			buy(this.silnik);
		}
		centre.appendChild( button );
	}
}

//
// getExpToNextLevelPercent()
// Return exp to the next level (in percent)
//
function getExpToNextLevelPercent(){
	return ((getExp()/getExpToNextLevel())*100).toFixed(2);
}

//
// updateProgressBar()
// Updates the progress bar
//
function updateProgressBar(){
	$(function() {
		$('.progressbar').each(function(){
			var t = $(this),
			dataperc = t.attr('data-perc'),
			barperc = Math.round(dataperc*5.56);
			t.find('.bar').animate({width:barperc}, dataperc*25);
		});
	});	
}

// End of file... ;)
// https://github.com/artur9010
// Do not copy :D